"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("firebase/app"));
require("firebase/firestore");
const firestorm = __importStar(require("firebase-firestorm"));
const firebaseconfiguration_1 = require("./firebaseconfiguration");
const firestore = app_1.default.initializeApp(firebaseconfiguration_1.firebaseConfig).firestore();
firestorm.initialize(firestore);
