"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_firestorm_1 = require("firebase-firestorm");
const customenumerations_1 = require("./customenumerations");
let Website = class Website extends firebase_firestorm_1.Entity {
};
__decorate([
    firebase_firestorm_1.subCollection({
        name: "further_custom_metadata",
        entity: customenumerations_1.CustomEnumerations,
    }),
    __metadata("design:type", Object)
], Website.prototype, "furtherCustomMetadata", void 0);
__decorate([
    firebase_firestorm_1.field({ name: "domain" }),
    __metadata("design:type", String)
], Website.prototype, "domain", void 0);
Website = __decorate([
    firebase_firestorm_1.rootCollection({
        name: "websites",
    })
], Website);
exports.Website = Website;
