"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_firestorm_1 = require("firebase-firestorm");
const clientdevicetype_1 = require("./clientdevicetype");
const feedbackmethod_1 = require("./feedbackmethod");
const predefinedsubject_1 = require("./predefinedsubject");
const sex_1 = require("./sex");
let Enumerations = class Enumerations extends firebase_firestorm_1.Entity {
};
__decorate([
    firebase_firestorm_1.subCollection({
        name: "client_device_type",
        entity: clientdevicetype_1.ClientDeviceType,
    }),
    __metadata("design:type", Object)
], Enumerations.prototype, "clientDeviceType", void 0);
__decorate([
    firebase_firestorm_1.subCollection({
        name: "feedback_method",
        entity: feedbackmethod_1.FeedbackMethod,
    }),
    __metadata("design:type", Object)
], Enumerations.prototype, "feedbackMethod", void 0);
__decorate([
    firebase_firestorm_1.subCollection({
        name: "predefined_subject",
        entity: predefinedsubject_1.PredefinedSubject,
    }),
    __metadata("design:type", Object)
], Enumerations.prototype, "predefinedSubject", void 0);
__decorate([
    firebase_firestorm_1.subCollection({
        name: "sex",
        entity: sex_1.Sex,
    }),
    __metadata("design:type", Object)
], Enumerations.prototype, "sex", void 0);
Enumerations = __decorate([
    firebase_firestorm_1.rootCollection({
        name: "enumerations",
    })
], Enumerations);
exports.Enumerations = Enumerations;
