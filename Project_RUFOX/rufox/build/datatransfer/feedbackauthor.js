"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_firestorm_1 = require("firebase-firestorm");
const name_1 = require("./name");
class FeedbackAuthor {
}
__decorate([
    firebase_firestorm_1.field({ name: "anonymous_user_uid" }),
    __metadata("design:type", String)
], FeedbackAuthor.prototype, "anonymousUserUID", void 0);
__decorate([
    firebase_firestorm_1.timestamp({ name: "birthdate" }),
    __metadata("design:type", Object)
], FeedbackAuthor.prototype, "birthdate", void 0);
__decorate([
    firebase_firestorm_1.field({ name: "email" }),
    __metadata("design:type", String)
], FeedbackAuthor.prototype, "email", void 0);
__decorate([
    firebase_firestorm_1.map({ name: "name" }),
    __metadata("design:type", name_1.Name)
], FeedbackAuthor.prototype, "name", void 0);
__decorate([
    firebase_firestorm_1.field({ name: "sex" }),
    __metadata("design:type", String)
], FeedbackAuthor.prototype, "sex", void 0);
exports.FeedbackAuthor = FeedbackAuthor;
