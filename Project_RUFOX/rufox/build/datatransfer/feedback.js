"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_firestorm_1 = require("firebase-firestorm");
const capture_1 = require("./capture");
const feedbackauthor_1 = require("./feedbackauthor");
const subject_1 = require("./subject");
let Feedback = class Feedback extends firebase_firestorm_1.Entity {
};
__decorate([
    firebase_firestorm_1.field({ name: "additional_message" }),
    __metadata("design:type", String)
], Feedback.prototype, "additionalMessage", void 0);
__decorate([
    firebase_firestorm_1.map({ name: "author" }),
    __metadata("design:type", feedbackauthor_1.FeedbackAuthor)
], Feedback.prototype, "author", void 0);
__decorate([
    firebase_firestorm_1.map({ name: "capture" }),
    __metadata("design:type", capture_1.Capture)
], Feedback.prototype, "capture", void 0);
__decorate([
    firebase_firestorm_1.field({ name: "client_device_type" }),
    __metadata("design:type", String)
], Feedback.prototype, "clientDeviceType", void 0);
__decorate([
    firebase_firestorm_1.field({ name: "method" }),
    __metadata("design:type", String)
], Feedback.prototype, "method", void 0);
__decorate([
    firebase_firestorm_1.timestamp({ name: "sent_on" }),
    __metadata("design:type", Object)
], Feedback.prototype, "sentOn", void 0);
__decorate([
    firebase_firestorm_1.map({ name: "subject" }),
    __metadata("design:type", subject_1.Subject)
], Feedback.prototype, "subject", void 0);
__decorate([
    firebase_firestorm_1.field({ name: "website" }),
    __metadata("design:type", String)
], Feedback.prototype, "website", void 0);
Feedback = __decorate([
    firebase_firestorm_1.rootCollection({
        name: "feedback",
    })
], Feedback);
exports.Feedback = Feedback;
