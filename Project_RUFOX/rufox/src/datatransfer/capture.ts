import { field } from "firebase-firestorm";

export class Capture {
    @field({ name: "url" })
    url!: URL
}