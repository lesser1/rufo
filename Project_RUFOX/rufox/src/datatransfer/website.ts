import { Entity, ICollection, field, rootCollection, subCollection } from "firebase-firestorm";

import { CustomEnumerations } from "./customenumerations";

@rootCollection({
    name: "websites",
})
export class Website extends Entity {
    @subCollection({
        name: "further_custom_metadata",
        entity: CustomEnumerations,
    })
    furtherCustomMetadata!: ICollection<CustomEnumerations>;

    @field({ name: "domain" })
    domain!: string;
}