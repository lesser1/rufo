import { field } from "firebase-firestorm";

export class Name {
    @field({ name: "first_name" })
    firstName!: string;

    @field({ name: "surname" })
    surname!: string;
}