import { Entity, ICollection, subCollection } from "firebase-firestorm";

import { CustomSubject } from "./customsubject";

export class CustomEnumerations extends Entity {
    @subCollection({
        name: "custom_subject",
        entity: CustomSubject,
    })
    customSubject!: ICollection<CustomSubject>;
}