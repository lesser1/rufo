import { Entity, ICollection, rootCollection, subCollection } from "firebase-firestorm";

import { ClientDeviceType } from "./clientdevicetype";
import { FeedbackMethod } from "./feedbackmethod";
import { PredefinedSubject } from "./predefinedsubject";
import { Sex } from "./sex";

@rootCollection({
    name: "enumerations",
})
export class Enumerations extends Entity {
    @subCollection({
        name: "client_device_type",
        entity: ClientDeviceType,
    })
    clientDeviceType!: ICollection<ClientDeviceType>;

    @subCollection({
        name: "feedback_method",
        entity: FeedbackMethod,
    })
    feedbackMethod!: ICollection<FeedbackMethod>;

    @subCollection({
        name: "predefined_subject",
        entity: PredefinedSubject,
    })
    predefinedSubject!: ICollection<PredefinedSubject>;

    @subCollection({
        name: "sex",
        entity: Sex,
    })
    sex!: ICollection<Sex>;
}