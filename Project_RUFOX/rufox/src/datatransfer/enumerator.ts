import { Entity, map } from "firebase-firestorm";

import { EnumeratorName } from "./enumeratorname";

export class Enumerator extends Entity {
    @map({ name: "name" })
    name!: EnumeratorName;
}