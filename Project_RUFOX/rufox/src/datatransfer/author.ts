import { Entity, ITimestamp, field, map, rootCollection, timestamp } from "firebase-firestorm";

import { Name } from "./name";

@rootCollection({
    name: "authors",
})
export class Author extends Entity {
    @field({ name: "anonymous_user_uid" })
    anonymousUserUID!: string;

    @timestamp({ name: "birthdate" })
    birthdate!: ITimestamp;

    @field({ name: "email" })
    email!: string;

    @map({ name: "name" })
    name!: Name;

    @field({ name: "sex" })
    sex!: string;
}