import { field } from "firebase-firestorm";

import { Enumerator } from "./enumerator";

export class FeedbackMethod extends Enumerator {
    @field({ name: "description" })
    description!: string;

    @field({ name: "hint" })
    hint!: string;
}