import { field } from "firebase-firestorm";

export class EnumeratorName {
    @field({ name: "alternative_names" })
    alternativeNames!: string[];

    @field({ name: "name" })
    name!: string;
}