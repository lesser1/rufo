import { ITimestamp, field, map, timestamp } from "firebase-firestorm";

import { Name } from "./name";

export class FeedbackAuthor {
    @field({ name: "anonymous_user_uid"})
    anonymousUserUID!: string;

    @timestamp({ name: "birthdate" })
    birthdate!: ITimestamp;

    @field({ name: "email"})
    email!: string;

    @map({ name: "name"})
    name!: Name;

    @field({ name: "sex"})
    sex!: string;
}