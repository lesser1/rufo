import { Entity, ITimestamp, rootCollection, field, map, timestamp } from "firebase-firestorm";

import { Capture } from "./capture";
import { FeedbackAuthor } from "./feedbackauthor";
import { Subject } from "./subject";

@rootCollection({
    name: "feedback",
})
export class Feedback extends Entity {
    @field({ name: "additional_message" })
    additionalMessage!: string;

    @map({ name: "author" })
    author!: FeedbackAuthor;

    @map({ name: "capture" })
    capture!: Capture;

    @field({ name: "client_device_type" })
    clientDeviceType!: string;

    @field({ name: "method" })
    method!: string;

    @timestamp({ name: "sent_on" })
    sentOn!: ITimestamp;

    @map({ name: "subject" })
    subject!: Subject;

    @field({ name: "website" })
    website!: string;
}