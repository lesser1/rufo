import { field } from "firebase-firestorm";

export class Subject {
    @field({ name: "additional_subject_specified_by_author"})
    additionalSubjectSpecifiedByAuthor!: string;

    @field({ name: "subject"})
    subject!: string[];
}