// Firebase App (the core Firebase SDK)
/// <reference lib="dom" />
import firebase from "firebase/app";

// Firebase products
// import "firebase/auth";
import "firebase/firestore";

// Firestorm: A mapper for firestore
import * as firestorm from "firebase-firestorm";

import { firebaseConfig } from "./firebaseconfiguration";
import { Author } from "./datatransfer/author";

const firestore = firebase.initializeApp(firebaseConfig).firestore();
firestorm.initialize(firestore);