
	"Document_c1_AusgewaehlteProjektartefakte"
	represents:
	Collection:	Name:	Document_c1_Ausgewählte Projektartefakte_Leonard Esser_2020-03-20;

	"Picture_c1_FirestoreDocumentTemplates"
	represents:
	Collection:	Name:	Picture_c1_Firebase Cloud Firestore document templates;

	"Project_RUFOX"
	represents:
	Folder:		Name:	Project_RUFO-X_Leonard Esser;

	"Website_NurMutZumTesten"
	represents:
	Folder:		Name:	Website_Nur Mut Zum Testen_Leonard Esser_2020-02-28;

	"Document_RUFOYFirebaseConfigScripts"
	represents:
	File:		Name:	Document_RUFO-Y Firebase configuration scripts_2020-03-10;

	"Presentation_RUFOProjektpraesentation_LeonardEsser"
	represents:
	File:		Name:	Presentation_RUFO -- Projektpräsentation_Leonard Esser_2020-03-02;